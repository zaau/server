const router = require('express').Router();

router.route('/').get((req,res) => {
    res.send({ok:true});
});
router.use('/box', require('./box/boxRouter'));
router.use('/notes', require('./notes/notesRouter'));
router.use('/users', require('./user/userRouter'));
//router.route('/timer', require('./timer/timerRouter'));
router.route("/camera", (req, res) => {
    
})

module.exports = router;

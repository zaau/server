var User = require('../user/userModel');
var signToken = require('./authHelper').signToken;

exports.signin = function(req, res, next){
    
    var token = signToken(req.user._id);
    res.json({token:token});
};
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var config = require('../../config/config');
var checkToken = expressJwt({secret:config.secrets.jwt});
var User = require('../user/userModel');

exports.decodeToken = function(){
    return function(req, res, next){
        if(req.headers.hasOwnProperty('access_token')){
            console.log(req.headers.access_token);
            req.headers.autorization = 'Bearer ' + req.headers.access_token;
        }
        
        checkToken(req, res, next);
    }
}

exports.getFreshUser = function(){
    return function(req, res, next){
        User.findById(req.user._id)
            .then(function(user){
                console.log("USERIS: ", user)
                if(!user){
                    res.status(401).send("Unautorized");
                }else{
                    req.user = user;
                    next();
                }
            },function(err){
                next(err);
            });
    };   
};

exports.verifyUser = function(){
    return function(req, res, next){
        var username = req.body.username;
        var password = req.body.password;
        
        if(!username || ! password){
            res.status(400).send("You need username and password");
            return;
        }
        
        User.findOne({username:username})
            .then(function(user){
                if(!user){
                    res.status(401).send('No user with the given username');
                }else{
                    if(!user.authenticate(password)){
                        res.status(401).send('Wrong password')
                    }else{
                        req.user = user;
                        console.log(req.user);
                        next();
                    }
                }
            },function(err){
                next(err);
            });
    };
};

exports.signToken = function(id){
    return jwt.sign(
        {_id:id},
        config.secrets.jwt,
        {expiresIn: config.expireTime}
    )
}

 /* // Sign the JWT
  return jwt.sign({
    sub: user.id,
    username: user.username,
    email: user.email,
    gravatar: createGravatarUrl(user.email),
    scope
  }, secret, { 
    algorithm: 'HS256',
    expiresIn: "1h" 
  });*/
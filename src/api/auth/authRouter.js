var router = require('express').Router();
var controller = require('./authController');
var verifyUser = require('./authHelper').verifyUser;

router.post('/signin', verifyUser(), controller.signin);
    
module.exports = router;
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var TagSchema = new Schema({name:String});
//also type could be [String]
var NotesScema = new Schema({
    name:{
        type:String,
        required:true,
        index:true,
        unique:true
    },
    shortDescription:{
        type:String,
        required:false,
        unique:false
    },
    longDescription:{
        type:String,
        required:false,
        unique:false
    },
    tags:{
        type:String,
        required:false,
        unique:false
    }
});
module.exports = mongoose.model('note',NotesScema);
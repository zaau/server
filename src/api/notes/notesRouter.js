var router = require('express').Router();
var createRoutes = require('../../util/createRoutes');
var controller = require("./notesController");

var auth = require('../auth/authHelper');
var checkUser = [auth.decodeToken(), auth.getFreshUser()];
//TODO Check where to use logger and write in readme about it
//createRoutes(controller, router);
//TODO use auth when needed
//TODO think then to use auth.getFresh user

router.param('id', controller.idParam);
router.param('name', controller.searchParam);

router.route('/')
    .get(checkUser, controller.get)
    .post(controller.post);

router.route('/search/:name')
    .get(controller.search);  

router.route('/:id')
    .get(controller.getOne)
    .delete(controller.delete)
    .put(controller.put);

 

module.exports = router;

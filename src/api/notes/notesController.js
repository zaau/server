var Note = require('./notesModel.js');

exports.idParam =  function(req, res, next, id){
    Note.findById(id)
        .then(function(note){
            if(!note){
                next(new Error('No note with id'));
            }else{
                req.note = note;
                next();
            }
        }, function(err){
            next(err);
        });
};

exports.searchParam =  function(req, res, next, name){
    Note.find({name: new RegExp(req.params.name, "i")})
        .then(function(notes){
            if(!notes){
                next(new Error('No note with name',req.name));
            }else{
                req.notes = notes;
                next();
            }
        }, function(err){
            next(err);
        });
};

exports.getOne = function(req, res, next){
    var note = req.note;
    console.log("NOTE: ", note)
    res.json(note);
};
exports.delete = function(req, res, next){
    req.note.remove(function(err, removedNote){
        if(!err){
            res.json(removedNote);
        }else{
            next(err);
        }
    });
};
exports.get = function(req,res,next){
    Note.find({})
        .then(function(notes){
            res.json(notes);
        },function(err){
            next(err);
        })
};

exports.post = function(req, res, next){
    var newNote = new Note(req.body);
    console.log(req.body);
    newNote.save(function(err, note){
        if(err){next(err);}
        res.json(note);
    });
};

exports.put = function(req, res, next) {
  var note = req.note;

  var update = req.body;
  console.log("UPDATE: ", update);
  Object.assign(note, update);
  note.save(function(err, saved) {
    if (err) {
      next(err);
    } else {
      res.json(saved);
    }
  })
};

exports.search = (req, res, next) =>{
    var notes = req.notes;
    res.json(notes);
}
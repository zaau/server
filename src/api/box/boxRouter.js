const router = require('express').Router();
var mongoose = require("mongoose");
var Schema = mongoose.Schema;


/*
{
    airHumidity:10%,
    groundHumidity:30%,
    temperature:20,
    fanSpeed:35

}
*/
/*
var TestSchema = new Schema({
    name:{
        type:String,
        required:true,
        index:true,
        unique:true
    }
});
var TestModel = mongoose.model('blah', TestSchema);

TestModel.collection.drop();


var testt = new TestModel({name:"Aurimas3"});
testt.save()
    .then(()=>{
        console.log("done")
    })
    .catch((err)=>{
        console.log("err: ", err)
    });

*/

var BoxSchema = new Schema({
    airHumidity:{
        type:String,
        required:true,
        index:true,
        unique:false
    },
    soilHumidity:{
        type:String,
        required:true,
        index:true,
        unique:false
    },
    temperature:{
        type: String,
        required:true,
        index:true,
        unique:false 
    },
    fanSpeed:{
        type: String,
        required:true,
        index:true,
        unique:false
    }
});

var BoxModel = mongoose.model("LoggingBox", BoxSchema);


function randomNumber(){
    var max = 100;
    var min = 1;
    return Math.random() * (max - min) + min;
}
function collectData(){
    return {
        airHumidity: randomNumber(),
        soilHumidity: randomNumber(),
        temperature: randomNumber(),
        fanSpeed: randomNumber()

    }
}
function createBoxModel(data, model){
    return new model(data);
}
function saveModelToDb(model){
    model.save()
        .then(() => {
            console.log("saved to db");
        })
        .catch((err) => {
            console.log("ERR: ", err);
        })
}

router.route('/start')
    .get( function(req, res){
        testInterval = setInterval( function(){
            saveModelToDb(createBoxModel(collectData(0), BoxModel));
        }, 2000)
        res.send({tracking:"true"})
    })
router.route('/stop')
    .get( function(req, res){
        clearInterval(testInterval)
        res.send({tracking:"false"})
})

module.exports = router;
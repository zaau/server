
var router = require('express').Router();
var controller = require('./userController');
//TODO Check where to use logger and write in readme about it
var auth = require('../auth/authHelper');
var checkUser = [auth.decodeToken(), auth.getFreshUser()];


////add/:a/:b req.params.a req.params.b
// add/?a/?b req.query.a req.query.b
//Post a= b= req.body.a req.body.b


router.param('id', controller.params);

router.get('/me', checkUser, controller.me);

router.route('/')
        .get(controller.get)
        .post(controller.post);
    
    router.route('/:id')
        .get(controller.getOne)
        .put(controller.put)
        .delete(checkUser, controller.delete);
        


module.exports = router;

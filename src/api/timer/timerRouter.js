const router = require('express').Router();
const Timer = require('./../../timer');

router.route("/post")
    .post((req, res, next) => {
    Timer.stopTimer();
    res.send({stopped:true});
})
router.route("/start").post((req, res) => {
    Timer.start();
    res.send({data: Timer.data});
})
router.route("/addInterval").post((req, res) => {
    console.log("REQ: ", req.body);
    //Timer.addInterval(1,3,[12,12]);
    /*const {data:{
        startDate,
        endDate,
        dayCycle
    }} = req;*/
    res.send("ok");
})

module.exports = router;
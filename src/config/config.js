var config = {
    dev:"development",
    prod:"production",
    port:3000,
    secrets:{
        jwt: process.env.JWT || 'simpleSecret'
    }
}


const NODE_ENV = process.env.NODE_ENV || config.dev;
config.env = NODE_ENV;

var envConfig;

try{
    envConfig = require('./' + config.env);
    envConfig = envConfig || {};
}
catch(e){
    envConfig = {};
}
var mergedConfig = Object.assign({}, config, envConfig);
module.exports = mergedConfig;
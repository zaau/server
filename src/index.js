const app = require('express')();
const config = require('./config/config');
var router = require('express').Router();
var api = require('./api/api');
var authRouter = require('./api/auth/authRouter');

require('mongoose').connect(config.db.url);
require('./middleware/apiMiddleware')(app);
var testInterval;


app.use("/", api);
app.use('/api/auth', authRouter);

if(config.seed){
    console.log("SEEEED!!!!")
    require('./util/seed');
}

app.listen(config.port, () => {
    console.log("###########################################" + config.env +"("+ config.port+")"  + "###########################################");
});
module.exports = function(controller, router){

router.param('id', controller.idParam);
router.param('name', controller.typeParam);
    router.route('/')
        .get(controller.get)
        .post(controller.post);
    
    router.route('/:id')
        .get(controller.getOne)
        .put(controller.put)
        .delete(controller.delete);
        
   router.route('/type/:name')
    .get(controller.getByType);
      
};
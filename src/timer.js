let setup = function(relay, helper){
    console.log(helper);
    var data = {
        stop:false,
        currentDate: 0,
        dayCycle: [18,6],
        stepper:0,
        status:false,
        intervals:[]
    }
    function init(changeDuration, dayCycle){
        data.dayCycle = dayCycle;
    }
    function addInterval(startDate, endDate, dayInterval){
        let blah = data.intervals.every( function(interval){ 
            console.log("start: ", startDate, " interval end: ", interval.endDate)
            return startDate > interval.endDate;
        });
        console.log(blah)
        if(!blah){
            return false;
        }
        
        
        let lastInterval = data.intervals[data.intervals.length -1] || data;
        let timeDiff = lastInterval.dayCycle[0] - dayInterval[0];
        //let timeDiff =  lastInterval.dayCycle[0] > dayInterval[0] ? lastInterval.dayCycle[0] - dayInterval[0] : dayInterval[0] - lastInterval.dayCycle[0]; 
        let timeStep = timeDiff / (endDate - startDate + 1) ;
        data.intervals.push({startDate:startDate, endDate: endDate, dayCycle: dayInterval, dayStep:Number(timeStep.toFixed(2))});
    }
    function updateDay(date){
        let activeInterval = data.intervals.filter( function(interval){
            //console.log("interval: ", interval, " date: ", date)
            return date >= interval.startDate && date <= interval.endDate;
        });
        if(activeInterval.length){
            console.log("activeInterval: ", activeInterval);
            data.dayCycle = activeInterval[0].dayCycle;
            data.stepper = activeInterval[0].dayStep;
        } else {
            data.stepper = 0;
        }


    }
    function start(dayCycle = data.dayCycle, currentDate = data.currentDate, interval = undefined){
       

        if(interval !== undefined){
            clearInterval(interval);
        }
        if(data.status){
            console.log("STOP!");
            return;
        }

        
        var timeOutTime = 0;
        var newInterval = dayCycle;
        if(helper.isDay()){
            helper.off();
            relay.on();
            timeOutTime= dayCycle[0];

            

        } else {
            data.currentDate += 1;
            updateDay(data.currentDate);
            var timeStep = data.stepper;
            newInterval = timeStep !== 0 
            ? dayCycle.map( function(time, i){
                return i % 2 ? time + timeStep : time - timeStep;   
            }) 
            : dayCycle;

            helper.on();
            relay.off();
            timeOutTime = dayCycle[1];
        }

        

        console.log("NEW INTERVAL: ", newInterval, " OLD INTERVAL: ", dayCycle, "TIMEOUT: ", timeOutTime, "DATE: ", data.currentDate);
        if(!data.stop){
            let dayInterval = setInterval(function(){
             start( newInterval, data.currentDate, dayInterval);
            },
            timeOutTime * 1000);
        }
        
    }
    function stopTimer(){
        data.stop = true;
    }

    return {
        start:start,
        init:init,
        data:data,
        addInterval:addInterval,
        stopTimer:stopTimer
    }
};



var dayCheck = function(){
        let status = true;
        return {
            isDay:function(){
                return status;
            },
            on:function(){
                status = true;
            },
            off:function(){
                status = false;
            }
        }
    }

var relay = function(){
    let status = false;
    return {
        isOn:function(){
            return status;
        },
        on:function(){
            status = true;
        },
        off:function(){
            status = false;
        }
    }
    
}
/*
{
                startDate:1,
                endDate:2,
                dayInterval:[12, 12],
                dayStep:6
            },
            {
                startDate:2,
                endDate:3,
                dayInterval:[18, 6],
                dayStep:-6
            }
*/

let Timer = setup(relay(), dayCheck());
module.exports = Timer;

//timer.start();

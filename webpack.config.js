var path = require('path');
var fs = require('fs');
var webpack = require('webpack');

var nodeModules = {};
fs.readdirSync('node_modules')
  .filter(function(x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .forEach(function(mod) {
    nodeModules[mod] = 'commonjs ' + mod;
  });


module.exports = env => {
    return {
        entry:'./src/index.js',
        target:'node',
        output:{
            path:path.join(__dirname, 'dist'),
            filename:'server.bundle.js'
        },
        externals: nodeModules,
        module:{
            rules:[
                {
                    test: /\.js$/, 
                    use:['babel-loader'],
                    exclude:/node-modules/
                }
            ]
        }
    }
}